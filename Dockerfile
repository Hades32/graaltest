FROM oracle/graalvm-ce:1.0.0-rc9 as graalvm
COPY ./ /app/
WORKDIR /app
RUN ./mvnw clean install -q ; rm -rf target/orig*.jar ; cp target/*.jar graaltest.jar
RUN java -cp graaltest.jar io.micronaut.graal.reflect.GraalClassLoadingAnalyzer
RUN native-image --no-server -J-ea \
             --class-path graaltest.jar \
			 -H:ReflectionConfigurationFiles=target/reflect.json \
			 -H:EnableURLProtocols=http,https \
			 -H:IncludeResources="logback.xml|application.yml|META-INF/services/*.*" \
			 -H:Name=graaltest \
			 -H:Class=graaltest.Application \
			 -H:+ReportUnsupportedElementsAtRuntime \
			 -H:+AllowVMInspection \
			 -H:-UseServiceLoaderFeature \
			 --rerun-class-initialization-at-runtime='sun.security.jca.JCAUtil$CachedSecureRandomHolder,javax.net.ssl.SSLContext' \
			 --delay-class-initialization-to-runtime=io.netty.handler.codec.http.HttpObjectEncoder,io.netty.handler.codec.http.websocketx.WebSocket00FrameEncoder,io.netty.handler.ssl.util.ThreadLocalInsecureRandom,com.sun.jndi.dns.DnsClient

FROM frolvlad/alpine-glibc
EXPOSE 8080
COPY --from=graalvm /app/graaltest .
ENTRYPOINT ["./graaltest"]
